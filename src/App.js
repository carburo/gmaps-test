import React from "react";
import {
  withScriptjs,
  withGoogleMap,
  GoogleMap,
  Marker
} from "react-google-maps";

const position = { lat: 41.9141816, lng: 12.5473798 };

class GMap extends React.Component {
  constructor(props) {
    super(props);
    this.state = { position };
    this.onMapClick = this.onMapClick.bind(this);
  }

  onMapClick(evt) {
    this.setState({
      position: evt.latLng
    });
    console.log(this.state);
  }

  render() {
    return (
      <GoogleMap defaultZoom={15} defaultCenter={position} onClick={this.onMapClick}>
        <Marker position={this.state.position} draggable={true} />
      </GoogleMap>
    );
  }
}

const MapWithScripts = withScriptjs(withGoogleMap(props => <GMap />));

const Map = () => (
  <MapWithScripts
    googleMapURL="https://maps.googleapis.com/maps/api/js?key=AIzaSyCgtHuGlKG9QrvEcW2tDBR_uMg1Z2WHqrw&v=3.exp&libraries=geometry,drawing,places"
    loadingElement={<div style={{ height: `100%` }} />}
    containerElement={<div style={{ height: `100vh` }} />}
    mapElement={<div style={{ height: `100%` }} />}
  />
);

export default Map;
